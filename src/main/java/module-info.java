/*
 * Utilities to integrate KosAPI and Syllabus Plus
 * Database entities
 * (c) 2018 FIT CTU (http://www.fit.cvut.cz)
 * Author: Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */

module cz.cvut.fit.kosapi2timetabler.timetabler_db {
    requires java.persistence;
    requires java.sql;
    exports cz.cvut.fit.kosapi2timetabler.timetabler_db;
}