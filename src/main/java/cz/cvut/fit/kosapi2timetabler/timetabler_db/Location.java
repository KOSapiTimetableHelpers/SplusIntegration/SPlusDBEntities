/*
 * Utilities to integrate KosAPI and Syllabus Plus
 * Database entities
 * (c) 2018 FIT CTU (http://www.fit.cvut.cz)
 * Author: Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */

package cz.cvut.fit.kosapi2timetabler.timetabler_db;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

/**
 * A room.
 * @author Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */
@Entity
public class Location implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private Long kosId;
    private String code;
    private Short capacity;
    
    public Location() {}

    public Location(final Long kosId, final String code, final short capacity) {
        this.kosId = kosId;
        this.code = code;
        this.capacity = capacity;
    }
    
    

    public Long getKosId() {
        return kosId;
    }
    public void setKosId(final long kosId) {
        this.kosId = kosId;
    }

    public Short getCapacity() {
        return capacity;
    }

    public void setCapacity(Short capacity) {
        this.capacity = capacity;
    }
    

    public String getCode() {
        return code == null ? "" : code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.kosId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Location other = (Location) obj;
        if (!Objects.equals(this.kosId, other.kosId)) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return "cz.cvut.fit.kosapi2db.entity.Location[ code=" + code + " ]";
    }
    
}
