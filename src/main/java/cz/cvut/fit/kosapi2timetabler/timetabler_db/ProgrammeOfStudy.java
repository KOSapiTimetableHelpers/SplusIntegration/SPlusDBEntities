/*
 * Utilities to integrate KosAPI and Syllabus Plus
 * Database entities
 * (c) 2018 FIT CTU (http://www.fit.cvut.cz)
 * Author: Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */

package cz.cvut.fit.kosapi2timetabler.timetabler_db;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

/**
 * A named set of mandatory and optional modules.
 * @author Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */
@Entity
public class ProgrammeOfStudy implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private String name;

    private String description;

    @ManyToMany
    @JoinTable(name = "programmeofstudy_mandatory", joinColumns = @JoinColumn(nullable = true))
    private Collection<Module> mandatoryModules;

    @ManyToMany
    @JoinTable(name = "programmeofstudy_optional", joinColumns = @JoinColumn(nullable = true))
    private Collection<Module> optionalModules;

    public Collection<Module> getOptionalModules() {
        return optionalModules;
    }

    public void setOptionalModules(Collection<Module> optionalModules) {
        this.optionalModules = optionalModules;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<Module> getMandatoryModules() {
        return mandatoryModules;
    }

    public void setMandatoryModules(Collection<Module> mandatoryModules) {
        this.mandatoryModules = mandatoryModules;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (name != null ? name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ProgrammeOfStudy)) {
            return false;
        }
        ProgrammeOfStudy other = (ProgrammeOfStudy) object;
        if ((this.name == null && other.name != null) || (this.name != null && !this.name.equals(other.name))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.cvut.fit.kosapi2timetabler.timetabler_db.ProgrammeOfStudy[ id=" + name + " ]";
    }

}
