/*
 * Utilities to integrate KosAPI and Syllabus Plus
 * Database entities
 * (c) 2018 FIT CTU (http://www.fit.cvut.cz)
 * Author: Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */

package cz.cvut.fit.kosapi2timetabler.timetabler_db;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"kosId"})})
public class Department implements Serializable {

    @OneToMany(mappedBy = "division")
    private List<Teacher> teachers;

    @Id
    private Long kosId;
    
    @Column(unique = true)
    private String code;
    private String title;
    
    public Department() {}

    public Department(Long kosId, String code, String title) {
        this.kosId = kosId;
        this.code = code;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getKosId() {
        return kosId;
    }

    public void setKosId(long id) {
        this.kosId = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (kosId != null ? kosId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Department)) {
            return false;
        }
        Department other = (Department) object;
        if ((this.kosId == null && other.kosId != null) || (this.kosId != null && !this.kosId.equals(other.kosId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.cvut.fit.kosapi2timetable.db.Department[ id=" + kosId + " ]";
    }
    
}
