/*
 * Utilities to integrate KosAPI and Syllabus Plus
 * Database entities
 * (c) 2018 FIT CTU (http://www.fit.cvut.cz)
 * Author: Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */

package cz.cvut.fit.kosapi2timetabler.timetabler_db;

import java.io.Serializable;
import java.time.DayOfWeek;
import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

/**
 * Single timetable slot (named ``rozvrhový lístek'' in KOS).
 * @author Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */
@Embeddable
public class TimetableSlot implements Serializable {
    /**
     * Week parity
     */
    public static enum Parity {
        ODD, EVEN, BOTH
    }
    public static final int MIN_HOUR = 1;
    public static final int MAX_HOUR = 16;
    @Enumerated(EnumType.STRING)
    private DayOfWeek dayOfWeek;
    private Byte firstHour = MIN_HOUR;
    private Byte duration;
    @ManyToOne(cascade = CascadeType.MERGE)
    private Availability parity;
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Location room;
    
    public TimetableSlot() {}

    public TimetableSlot(DayOfWeek day, Byte firstHour, Byte duration, Availability parity) {
        this.dayOfWeek = day;
        this.firstHour = firstHour;
        this.duration = duration;
        this.parity = parity;
    }

    public Location getRoom() {
        return room;
    }

    public void setRoom(Location room) {
        this.room = room;
    }

    public DayOfWeek getDay() {
        return dayOfWeek;
    }

    public void setDay(DayOfWeek day) {
        this.dayOfWeek = day;
    }

    public Byte getFirstHour() {
        return firstHour;
    }

    public void setFirstHour(byte firstHour) {
        if (firstHour >= MIN_HOUR && firstHour + duration <= MAX_HOUR)
            this.firstHour = firstHour;
        else
            throw new IllegalArgumentException("First hour not valid: " + firstHour);
    }

    public Byte getDuration() {
        return duration;
    }

    public void setDuration(final byte duration) {
        if (duration > 0 && firstHour + duration <= MAX_HOUR)
            this.duration = duration;
        else
            throw new IllegalArgumentException("Duration not valid: " + duration + " firstHour: " + firstHour + " of: " + this.toString());
    }

    public Availability getParity() {
        return parity;
    }

    public void setParity(Availability parity) {
        this.parity = parity;
    }
    
    public boolean isOverlappingWith(final TimetableSlot other) {
        return dayOfWeek.equals(other.dayOfWeek) &&
                (firstHour >= other.firstHour && firstHour <= other.firstHour + other.duration ||
                firstHour + duration >= other.firstHour && firstHour + duration <= other.firstHour + other.duration ||
                firstHour <= other.firstHour && firstHour + duration >= other.firstHour
                );
    }

    @Override
    public String toString() {
        return "TimetableSlot{" + "day=" + dayOfWeek + ", firstHour=" + firstHour + ", duration=" + duration + ", parity=" + parity + ", room " + room + '}';
    }
}