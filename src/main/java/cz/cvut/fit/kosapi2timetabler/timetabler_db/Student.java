/*
 * Utilities to integrate KosAPI and Syllabus Plus
 * Database entities
 * (c) 2018 FIT CTU (http://www.fit.cvut.cz)
 * Author: Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */

package cz.cvut.fit.kosapi2timetabler.timetabler_db;

import cz.cvut.fit.kosapi2timetabler.timetabler_db.StudentSet;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

/**
 *
 * @author Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */
@Entity
public class Student implements Serializable {
    @ManyToMany(mappedBy = "students")
    private List<StudentSet> studentSets;
    private static final long serialVersionUID = 1L;
    @Id
    private String personalNumber;
    
    private String name;
    private String description;
    private String email;
    
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personalNumber != null ? personalNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Student)) {
            return false;
        }
        Student other = (Student) object;
        if ((this.personalNumber == null && other.personalNumber != null) || (this.personalNumber != null && !this.personalNumber.equals(other.personalNumber))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.cvut.fit.kosapi2db.entity.Student[ id=" + personalNumber + " ]";
    }
    
}
