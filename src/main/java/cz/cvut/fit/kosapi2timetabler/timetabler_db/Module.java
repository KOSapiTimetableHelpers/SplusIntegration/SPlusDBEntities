/*
 * Utilities to integrate KosAPI and Syllabus Plus
 * Database entities
 * (c) 2018 FIT CTU (http://www.fit.cvut.cz)
 * Author: Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */

package cz.cvut.fit.kosapi2timetabler.timetabler_db;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * A course/subject/module.
 * @author Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */
@Entity
public class Module implements Serializable {

    private static final long serialVersionUID = 1L;

    @OneToOne(mappedBy = "course")
    private StudentSet studentSetLecture;

    @Id
    private Long kosId;

    private String code;

    private String title;

    private Short capacity;
    private Short realSize;
    private Short tutorialCapacity;

    /**
     * A week pattern. E.g., for an even week: "0101010101010".
     */
    private String weekPattern;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Student> students;

    @OneToOne
    private Suitability suitabilityLecture;

    @OneToOne
    private Suitability suitabilityTutorial;

    @ManyToOne
    private Department department;

    public Module() {
    }

    public Module(Long kosId, String code, String title, Short capacity, Short realSize, Short tutorialCapacity, Department department, String weekPattern) {
        this.kosId = kosId;
        this.code = code;
        this.title = title;
        this.capacity = capacity;
        this.realSize = realSize;
        this.tutorialCapacity = tutorialCapacity;
        this.department = department;
        this.weekPattern = weekPattern;
    }

    public String getWeekPattern() {
        return weekPattern;
    }

    public void setWeekPattern(String weekPattern) {
        this.weekPattern = weekPattern;
    }

    public Long getKosId() {
        return kosId;
    }

    public void setKosId(Long kosId) {
        this.kosId = kosId;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Suitability getSuitabilityLecture() {
        return suitabilityLecture;
    }

    public void setSuitabilityLecture(Suitability suitabilityLecture) {
        this.suitabilityLecture = suitabilityLecture;
    }

    public Suitability getSuitabilityTutorial() {
        return suitabilityTutorial;
    }

    public void setSuitabilityTutorial(Suitability suitabilityTutorial) {
        this.suitabilityTutorial = suitabilityTutorial;
    }

    public Short getTutorialCapacity() {
        return tutorialCapacity;
    }

    public void setTutorialCapacity(Short tutorialCapacity) {
        this.tutorialCapacity = tutorialCapacity;
    }

    public Short getRealSize() {
        return realSize;
    }

    public void setRealSize(Short realSize) {
        this.realSize = realSize;
    }

    public StudentSet getStudentSetLecture() {
        return studentSetLecture;
    }

    public void setStudentSetLecture(StudentSet studentSetLecture) {
        this.studentSetLecture = studentSetLecture;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    public void setKosId(long id) {
        this.kosId = id;
    }

    public Short getCapacity() {
        return capacity;
    }

    public void setCapacity(Short capacity) {
        this.capacity = capacity;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.kosId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Module other = (Module) obj;
        if (!Objects.equals(this.kosId, other.kosId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.cvut.fit.kosapi2db.entity.Module[ id=" + kosId + " code= " + code + "]";
    }

}
