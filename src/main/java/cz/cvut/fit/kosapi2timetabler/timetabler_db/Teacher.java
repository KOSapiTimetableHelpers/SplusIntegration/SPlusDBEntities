/*
 * Utilities to integrate KosAPI and Syllabus Plus
 * Database entities
 * (c) 2018 FIT CTU (http://www.fit.cvut.cz)
 * Author: Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */

package cz.cvut.fit.kosapi2timetabler.timetabler_db;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

/**
 *
 * @author guthondr
 */
@Entity
public class Teacher implements Serializable {
    @ManyToMany(mappedBy = "teachers",cascade = CascadeType.PERSIST)
    private List<Suitability> suitabilities;
    private static final long serialVersionUID = 1L;
    @Id
    private Long kosId;
    private Integer personalNumber;
    private String name;
    private String username;
    private String email;
    
    @ManyToOne
    private Department division;

    public Teacher() {}

    public Teacher(Long kosId, Integer personalNumber, String name, String username, String email, Department division) {
        this.kosId = kosId;
        this.personalNumber = personalNumber;
        this.name = name;
        this.username = username;
        this.email = email;
        this.division = division;
    }
    
    
    
    public Long getKosId() {
        return kosId;
    }

    public void setKosId(long kosId) {
        this.kosId = kosId;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(Integer personalNumber) {
        this.personalNumber = personalNumber;
    }

    public Department getDivision() {
        return division;
    }

    public void setDivision(Department division) {
        this.division = division;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.kosId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Teacher other = (Teacher) obj;
        if (!Objects.equals(this.kosId, other.kosId)) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return "cz.cvut.fit.kosapi2db.entity.Teacher[ id=" + kosId + " username=" + username + "]";
    }
    
}
