/*
 * Utilities to integrate KosAPI and Syllabus Plus
 * Database entities
 * (c) 2018 FIT CTU (http://www.fit.cvut.cz)
 * Author: Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */

package cz.cvut.fit.kosapi2timetabler.timetabler_db;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Single timetable slot. In KOS, one parallel consists of (possibly) multiple timetable slots.
 * @author Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"kosId"})})
public class Activity implements Serializable {

    @Id
    private Long kosId;
    
    private String name;

    @ManyToOne
    private Module module;
    
    @ManyToOne
    private ActivityType activityType;

    private Short plannedSize;

    @ManyToOne
    private Suitability suitability;

    @OneToOne(cascade = CascadeType.PERSIST)
    private StudentSet studentSet;
    
    @Embedded
    private TimetableSlot timetableSlot;
    
    @ManyToOne
    private Teacher teacher1;

    @ManyToOne
    private Teacher teacher2;

    public Teacher getTeacher2() {
        return teacher2;
    }

    public void setTeacher2(Teacher teacher2) {
        this.teacher2 = teacher2;
    }

    public Teacher getTeacher1() {
        return teacher1;
    }

    public void setTeacher1(Teacher teacher1) {
        this.teacher1 = teacher1;
    }

    public Activity() {}
    
    public Activity(final Module module) {
        this.module = module;
    }
    
    public void generateName() {
        this.name = String.format("%s/%s/%s", module.getCode(), activityType.getId(), "01");
    }

    public StudentSet getStudentSet() {
        return studentSet;
    }

    public void setStudentSet(StudentSet studentSet) {
        this.studentSet = studentSet;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }

    public Short getPlannedSize() {
        return plannedSize;
    }

    public void setPlannedSize(Short plannedSize) {
        this.plannedSize = plannedSize;
    }

    public Suitability getSuitability() {
        return suitability;
    }

    public void setSuitability(Suitability suitability) {
        this.suitability = suitability;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getKosId() {
        return kosId;
    }

    public void setKosId(Long kosId) {
        this.kosId = kosId;
    }

    public TimetableSlot getTimetableSlot() {
        if (timetableSlot == null)
            timetableSlot = new TimetableSlot();
        return timetableSlot;
    }

    public void setTimetableSlot(TimetableSlot timetableSlot) {
        this.timetableSlot = timetableSlot;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.kosId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Activity other = (Activity) obj;
        if (!Objects.equals(this.kosId, other.kosId)) {
            return false;
        }
        return true;
    }


    @Override
    public String toString() {
        return "Activity{" + "kosId=" + kosId + ", name=" + name + ", module=" + module + ", activityType=" + activityType + ", plannedSize=" + plannedSize + ", suitability=" + suitability + ", studentSet=" + studentSet + ", timetableSlot=" + timetableSlot + '}';
    }

}
