/*
 * Utilities to integrate KosAPI and Syllabus Plus
 * Database entities
 * (c) 2018 FIT CTU (http://www.fit.cvut.cz)
 * Author: Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */

package cz.cvut.fit.kosapi2timetabler.timetabler_db;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * A named availability of a location, an activity, a teacher, etc. (splus). Typically parity of a week.
 * @author guthondr
 */
@Entity
public class Availability implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private String id;

    /**
     * A week pattern. E.g., for an even week: "0101010101010".
     */
    private String pattern;

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public Availability() {
    }

    public Availability(final String id, final String pattern) {
        this.id = id;
        this.pattern = pattern;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Availability)) {
            return false;
        }
        Availability other = (Availability) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "cz.cvut.fit.kosapi2timetable.db.Availability[ id=" + id + " ]";
    }

}
