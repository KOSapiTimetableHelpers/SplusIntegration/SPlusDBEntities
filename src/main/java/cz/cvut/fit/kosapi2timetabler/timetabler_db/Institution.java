/*
 * Utilities to integrate KosAPI and Syllabus Plus
 * Database entities
 * (c) 2018 FIT CTU (http://www.fit.cvut.cz)
 * Author: Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */

package cz.cvut.fit.kosapi2timetabler.timetabler_db;

import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * A university/faculty settings in splus.
 * @author Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */
@Entity
public class Institution implements Serializable {
    @Id
    private String id;
    
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Availability namedAvailability;

    private Date startDate;
    
    private String description;
    private String weekPattern;

    public Institution(String id, Availability namedAvailability, Date startDate, String description, String weekPattern) {
        this.id = id;
        this.namedAvailability = namedAvailability;
        this.startDate = startDate;
        this.description = description;
        this.weekPattern = weekPattern;
    }

    public Institution() {}
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Availability getNamedAvailability() {
        return namedAvailability;
    }

    public void setNamedAvailability(Availability namedAvailability) {
        this.namedAvailability = namedAvailability;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWeekPattern() {
        return weekPattern;
    }

    public void setWeekPattern(String weekPattern) {
        this.weekPattern = weekPattern;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Institution other = (Institution) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
   
}
