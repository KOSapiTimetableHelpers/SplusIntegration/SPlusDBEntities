/*
 * Utilities to integrate KosAPI and Syllabus Plus
 * Database entities
 * (c) 2018 FIT CTU (http://www.fit.cvut.cz)
 * Author: Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */

package cz.cvut.fit.kosapi2timetabler.timetabler_db;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author guthondr
 */
@Entity
@Table(name = "Suitability")
public class Suitability implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private String id;
    
    @OneToOne(cascade = CascadeType.MERGE)
    private Module course;
    
    @ManyToMany(cascade = CascadeType.MERGE)
    private Set<Teacher> teachers;
    
    @ManyToMany
    @JoinTable(name = "suitability_location_primary", joinColumns = @JoinColumn(nullable = true))
    private Set<Location> primaryLocations;

    @ManyToMany
    @JoinTable(name = "suitability_location_other", joinColumns = @JoinColumn(nullable = true))
    private Set<Location> otherLocations;

    public Suitability() {
    }

    public Suitability(final String id, final Set<Location> primaryLocs) {
        this(id, primaryLocs, null);
    }
    
    public Suitability(final String id, final Set<Location> primaryLocs, final Set<Location> othLocs) {
        this.id = id;
        this.primaryLocations = primaryLocs;
        this.otherLocations = othLocs;
    }
    
    

    public Module getModule() {
        return course;
    }

    public void setModule(Module module) {
        this.course = module;
    }

    public Set<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(Set<Teacher> teachers) {
        this.teachers = teachers;
    }
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Set<Location> getPrimaryLocations() {
        return primaryLocations;
    }

    public void setPrimaryLocations(Set<Location> primaryLocations) {
        this.primaryLocations = primaryLocations;
    }

    public Set<Location> getOtherLocations() {
        return otherLocations;
    }

    public void setOtherLocations(Set<Location> otherLocations) {
        this.otherLocations = otherLocations;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Suitability)) {
            return false;
        }
        Suitability other = (Suitability) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.cvut.fit.kosapi2db.entity.Suitability[ id=" + id + " ]";
    }
    
}
