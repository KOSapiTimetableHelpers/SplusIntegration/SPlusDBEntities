/*
 * Utilities to integrate KosAPI and Syllabus Plus
 * Database entities
 * (c) 2018 FIT CTU (http://www.fit.cvut.cz)
 * Author: Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */

package cz.cvut.fit.kosapi2timetabler.timetabler_db;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * A type of an activity (e.g., a lecture).
 * @author Ondrej Guth (ondrej.guth@fit.cvut.cz)
 */
@Entity
public class ActivityType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private String id;

    public String getId() {
        return id;
    }
    
    public ActivityType() {}
    
    public ActivityType(final String title) {
        id = title;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ActivityType)) {
            return false;
        }
        ActivityType other = (ActivityType) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cz.cvut.fit.kosapi2db.entity.ActivityType[ id=" + id + " ]";
    }
    
}
