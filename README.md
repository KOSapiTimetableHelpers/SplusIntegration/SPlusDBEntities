JPA entities for KOSapi-Splus integration
=================================

![pipeline status](https://gitlab.fit.cvut.cz/KOSapiTimetableHelpers/SplusIntegration/SPlusDBEntities/badges/master/pipeline.svg)


About
-----

This project (library) contains [Java Persistence API](https://jakarta.ee/specifications/persistence/) entities representing database tables to be loaded by Scientia Syllabus Plus course planner application. Loading the data is not the job of this project.

License
-------

Copyright Ondřej Guth, 2019, [Faculty of Information Technology](https://fit.cvut.cz) of [Czech Technical University in Prague](https://www.cvut.cz/).
